from django.db import models


# Main
class Header(models.Model):
    text1 = models.CharField(max_length=100, blank=True, null=True)
    text2 = models.CharField(max_length=100, blank=True, null=True)
    picture = models.ImageField(upload_to='header')


class Services(models.Model):
    service_type = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.service_type


class Body(models.Model): #TextImage
    text1 = models.CharField(max_length=100, blank=True, null=True)
    text2 = models.CharField(max_length=100, blank=True, null=True)
    picture = models.ImageField(upload_to='body')


class Contact(models.Model):
    phone_number1 = models.CharField(max_length=30, null=True, blank=True)
    phone_number2 = models.CharField(max_length=30, null=True, blank=True)
    email = models.EmailField()


class OurTeam(models.Model):
    name = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    image = models.ImageField(upload_to='team')

    def __str__(self):
        return self.name


# carrier


class Truckload(models.Model):
    type = models.TextField()

    def __str__(self):
        return self.type


class Truck(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name


class Carrier(models.Model):
    name = models.CharField(max_length=100)
    company_name = models.TextField()
    truckload_type = models.ForeignKey(Truckload, on_delete=models.SET_NULL, null=True, blank=True)
    truck_type = models.ForeignKey(Truck, on_delete=models.SET_NULL, null=True, blank=True)
    pickup_date = models.DateField()
    deliever_date = models.DateField()
    from_city = models.CharField(max_length=100)
    to_city = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)
    email = models.EmailField()
    comments = models.TextField(blank=True)

    def __str__(self):
        return f'carrier name : {self.name}'


class Cargotype(models.Model):
    type = models.TextField()

    def __str__(self):
        return self.type


class Weight(models.Model):
    weight = models.CharField(max_length=100)

    def __str__(self):
        return self.weight


class Shipper(models.Model):
    name = models.CharField(max_length=100)
    company_name = models.TextField()
    truckload_type = models.ForeignKey(Truckload, on_delete=models.SET_NULL, null=True, blank=True)
    truck = models.ForeignKey(Truck, on_delete=models.SET_NULL, null=True, blank=True)
    cargo_type = models.ForeignKey(Cargotype, on_delete=models.SET_NULL, null=True, blank=True)
    cargo_weight = models.ForeignKey(Weight, on_delete=models.SET_NULL, null=True, blank=True)
    pickup_date = models.DateField()
    deliever_date = models.DateField()
    from_city = models.CharField(max_length=100)
    to_city = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)
    email = models.EmailField()
    comments = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'shipper name : {self.name}'


class Statistics(models.Model):
    text = models.TextField()
    number = models.CharField(max_length=100)


class Vacansy(models.Model):
    position = models.CharField(max_length=100)
    company = models.CharField(max_length=100)
    time = models.CharField(max_length=100)
    payment = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.CharField(max_length=100)
    location = models.CharField(max_length=100)

    def __str__(self):
        return self.position


class Privacy(models.Model):
    text = models.TextField()


class FAQ(models.Model):
    question = models.TextField()
    answer = models.TextField()


class FormSubmit(models.Model):
    status_code = models.IntegerField()
    text1 = models.TextField()
    text2 = models.TextField()
