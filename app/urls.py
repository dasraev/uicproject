from rest_framework import routers
from app.views import *
from django.urls import path

router = routers.DefaultRouter()

router.register(r'services', ServicesViewSet, basename='services')
router.register(r'header', HeaderViewSet, basename='header')
router.register(r'body', BodyViewSet, basename='body')
router.register(r'contacts', ContactViewSet, basename='contacts')
router.register(r'carrier_form', CarrierView, basename='carrier_form')
router.register(r'shipper_form', ShipperView, basename='shipper_form')
router.register(r'team_members', TeamViewSet, basename='team_members')
router.register(r'vacansies', VacansyViewSet, basename='vacansies')
router.register(r'privacy', PrivacyViewSet, basename='privacy')
router.register(r'faq', FAQViewSet, basename='faq')
