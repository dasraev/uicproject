from rest_framework.viewsets import ModelViewSet
from .models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.shortcuts import render
from rest_framework.pagination import PageNumberPagination
from app.utils import render_to_pdf
from django.http import HttpResponse


def shipper_pdf(request, id):
    data = Shipper.objects.get(id=id)
    pdf = render_to_pdf('app/shipper.html', {'context': data})
    return HttpResponse(pdf, content_type='application/pdf')


def carrier_pdf(request, id):
    data = Carrier.objects.get(id=id)
    pdf = render_to_pdf('app/carrier.html', {'context': data})
    return HttpResponse(pdf, content_type='application/pdf')


def index(request):
    return render(request, 'app/index.html')


# class ServiceGetView(APIView):
#     def get(self,request,format=None):
#         services = Services.objects.all()
#         serializer = ServiceSerializer(services,many=True)
#         return Response(serializer.data)

class ServicesViewSet(ModelViewSet):
    queryset = Services.objects.all()
    serializer_class = ServiceSerializer

    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)


class ContactViewSet(ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)


class CarrierView(ModelViewSet):
    queryset = Carrier.objects.all()
    serializer_class = CarrierSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.validated_data
            carrier = Carrier.objects.create(
                name=data['name'],
                company_name=data['company_name'],
                pickup_date=data['pickup_date'],
                deliever_date=data['deliever_date'],
                from_city=data['from_city'],
                to_city=data['to_city'],
                phone_number=data['phone_number'],
                email=data['email'],
                comments=data['comments'],
                truckload_type=data['truckload_type'],
                truck_type=data['truck_type'],
            )
            carrier.save()

        formsubmit = FormSubmit.objects.get(status_code=200)
        serializer = FormSubmitSerializer(formsubmit)
        data = {
            'text1': serializer.data['text1'],
            'text2': serializer.data['text2']
        }
        return Response(status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)


class ShipperView(ModelViewSet):
    queryset = Shipper.objects.all()
    serializer_class = ShipperSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            data = serializer.validated_data
            shipper = Shipper.objects.create(
                name=data['name'],
                company_name=data['company_name'],
                truckload_type=data['truckload_type'],
                truck=data['truck'],
                cargo_type=data['cargo_type'],
                cargo_weight=data['cargo_weight'],
                pickup_date=data['pickup_date'],
                deliever_date=data['deliever_date'],
                from_city=data['from_city'],
                to_city=data['to_city'],
                phone_number=data['phone_number'],
                email=data['email'],
                comments=data['comments'],
            )
            shipper.save()
        formsubmit = FormSubmit.objects.get(status_code=200)
        serializer = FormSubmitSerializer(formsubmit)
        data = {
            'text1': serializer.data['text1'],
            'text2': serializer.data['text2']
        }
        return Response(data)

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)


class TeamViewSet(ModelViewSet):
    queryset = OurTeam.objects.all()
    serializer_class = OurTeamSerializer

    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)


class HeaderViewSet(ModelViewSet):
    queryset = Header.objects.all()
    serializer_class = HeaderSerializer

    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

class BodyViewSet(ModelViewSet):
    queryset = Body.objects.all()
    serializer_class = BodySerializer

    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 4
    page_size_query_param = 'page_size'

class VacansyViewSet(ModelViewSet):
    queryset = Vacansy.objects.all()
    serializer_class = VacansySerializer
    pagination_class = StandardResultsSetPagination
    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)


class PrivacyViewSet(ModelViewSet):
    queryset = Privacy.objects.all()
    serializer_class = PrivacySerializer

    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)


class FAQViewSet(ModelViewSet):
    queryset = FAQ.objects.all()
    serializer_class = FAQSerializer
    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)


class StatisticsViewSet(ModelViewSet):
    queryset = Statistics.objects.all()
    serializer_class = StatisticSerializer

    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)
