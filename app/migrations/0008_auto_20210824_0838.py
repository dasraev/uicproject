# Generated by Django 3.2.6 on 2021-08-24 08:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_alter_statistics_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='Body',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text1', models.CharField(blank=True, max_length=100, null=True)),
                ('text2', models.CharField(blank=True, max_length=100, null=True)),
                ('picture', models.ImageField(upload_to='body')),
            ],
        ),
        migrations.CreateModel(
            name='Header',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text1', models.CharField(blank=True, max_length=100, null=True)),
                ('text2', models.CharField(blank=True, max_length=100, null=True)),
                ('picture', models.ImageField(upload_to='header')),
            ],
        ),
        migrations.DeleteModel(
            name='TextImage',
        ),
    ]
