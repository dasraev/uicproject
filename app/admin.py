from django.contrib import admin
from .models import *
from django.urls import reverse
from django.http import HttpResponseRedirect

@admin.register(Shipper)
class ShipperAdmin(admin.ModelAdmin):

    change_form_template = "app/shipper_changeform.html"
    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.object_id = object_id
        print(self.object_id)
        return self.changeform_view(request, object_id, form_url, extra_context)

    def response_change(self, request, obj):
        if "pdf" in request.POST:
            return HttpResponseRedirect(reverse('shipper',args=[self.object_id]))
        return super().response_change(request, obj)




@admin.register(Carrier)
class CarrierAdmin(admin.ModelAdmin):

    change_form_template = "app/shipper_changeform.html"
    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.object_id = object_id
        print(self.object_id)
        return self.changeform_view(request, object_id, form_url, extra_context)

    def response_change(self, request, obj):
        if "pdf" in request.POST:
            return HttpResponseRedirect(reverse('carrier',args=[self.object_id]))

        return super().response_change(request, obj)






# admin.site.register(Carrier)
admin.site.register(Services)
# admin.site.register(Shipper)
admin.site.register(Truck)
admin.site.register(Truckload)
admin.site.register(Cargotype)
admin.site.register(Weight)
admin.site.register(Header)
admin.site.register(Body)
admin.site.register(Statistics)
admin.site.register(Contact)
admin.site.register(Vacansy)
admin.site.register(FAQ)
admin.site.register(FormSubmit)
admin.site.register(OurTeam)

