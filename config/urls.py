from django.contrib import admin
from django.urls import path
from app.views import *
from django.urls import path,include
from app.urls import router
from django.conf.urls.static import static
from django.conf import settings
from app.views import index
from .yasg import urlpatterns as doc_urls
from app import views
urlpatterns = [
    # path('a/',index,name='index'),
    path('admin/', admin.site.urls),
    path('pdf-generate/shipper/<int:id>/', views.shipper_pdf, name='shipper'),
    path('pdf-generate/carrier/<int:id>/', views.carrier_pdf, name='carrier'),
    path('',include(router.urls))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += doc_urls
